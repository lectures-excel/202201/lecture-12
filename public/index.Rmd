---
pagetitle: "Análisis de datos"
title: "\nTaller de excel\n"
subtitle: "\nLectura 12: Análisis de datos\n"
author: "\nEduard F Martínez-González\n"
date: "Universidad de los Andes | [ECON-1300](https://eduard-martinez.github.io/teaching/excel/2022-01/website.html)"
output: 
  revealjs::revealjs_presentation:  
    theme: simple 
    highlight: tango
    center: true
    nature:
      transition: slide
      self_contained: false # para que funcione sin internet
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: true
      showSlideNumber: 'all'
    seal: true # remover la primera diapositiva (https://github.com/yihui/xaringan/issues/84)
    # Help 1: https://revealjs.com/presentation-size/  
    # Help 2: https://bookdown.org/yihui/rmarkdown/revealjs.html
    # Estilos revealjs: https://github.com/hakimel/reveal.js/blob/master/css/theme/template/theme.scss
---

## Hoy veremos...

### **[1.]** Configuración inicial

### **[2.]** Correlación

### **[3.]** Análisis de correlación

<!----------------------------->
<!--- Configuración inicial --->
<!----------------------------->
# [1.] Configuración inicial
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!----------------------->
## En Windows
![](help/pics/data_analysis_windows.gif)

<!----------------------->
## En Mac
![](help/pics/data_analysis_mac.gif)

<!------------------->
<!--- Correlación --->
<!------------------->
# [2.] Correlación
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!----------------------->
## Correlación simple
![](help/pics/correlacion.gif)

<!----------------------->
## Matriz de correlación
![](help/pics/matriz_correlacion.gif)

<!----------------------------->
<!--- Análisis de regresión --->
<!----------------------------->
# [3.] Análisis de regresión
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!----------------------->
## Regresión
![](help/pics/regresion.gif)

<!------------>
<!--- Task --->
<!------------>

# [5.] Task
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

* **1.** Use el archivo "task.xlsx" de la carpeta [datos](https://uniandes-my.sharepoint.com/:f:/g/personal/ef_martinezg_uniandes_edu_co/Ek_BGC6lL9FDnCDh4mgGVEwB7PEWmEKadh5NsTIJpwwE-g?e=Pq9nol).
* **2.** Use la pestaña "Data" para responder las preguntas de la pestaña "Task".
* **3.** Guarde el libro en formato .xlsx, asígnele su código como nombre del archivo y súbalo al enlace de OneDrive para el task de la clase. Por ejemplo: **201725842.xlsx**

Nota: no seguir las isntrucciones tiene una penalización del 30% de la nota.


<!--------------------->
<!--- Gracias --->
<!--------------------->
# Gracias
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## Hoy vimos...

☑ Configuración inicial

☑ Correlación

☑ Análisis de regresion


<!--- HTML style --->
<style type="text/css">
.reveal .progress {background: #CC0000 ; color: #CC0000}
.reveal .controls {color: #CC0000}
.reveal h1.title {font-size: 2.4em;color: #CC0000; font-weight: bolde}
.reveal h1.subtitle {font-size:2.0em ; color:#000000}
.reveal section h1 {font-size:2.0em ; color:#CC0000 ; font-weight:bolder ; vertical-align:middle}
.reveal section h2 {font-size:1.3em ; color:#CC0000 ; font-weight:bolder ; text-align:left}
.reveal section h3 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section h4 {font-size:0.9em ; color:#CC0000 ; text-align:left}
.reveal section h5 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section p {font-size:0.7em ; color:#00000 ; text-align:left}
.reveal section a {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section href {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section div {align="center";}
.reveal ul {list-style-type:disc ; font-size:0.8em ; color:#00000 ; display: block;}
.reveal ul ul {list-style-type: square; font-size:0.8em ; display: block;}
.reveal ul ul ul {list-style-type: circle; font-size:0.8em ; display: block;}
.reveal section img {display: inline-block; border: 0px; background-color: #FFFFFF; align="center"}
</style>




